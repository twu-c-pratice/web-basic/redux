import {combineReducers} from 'redux';
import productReducer from './product/reducers/productReducer';
import userReducer from "./user/reducers/userReducer";

export default combineReducers({
  product: productReducer,
  user: userReducer
});