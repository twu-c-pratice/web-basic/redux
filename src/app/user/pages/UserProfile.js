import React, {Component} from 'react';
import {connect} from "react-redux";
import getUserProfileById from "../actions/userActions";

class UserProfile extends Component {

  componentDidMount() {
    this.props.getUserProfileById(this.props.match.params.id);
  }

  render() {
    const {name, gender, description} = this.props.userProfile;

    return (
      <div>
        <h1>User Profile</h1>
        <ul>
          <li>User Name: {name}</li>
          <li>Gender: {gender}</li>
          <li>Description: {description}</li>
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userProfile: state.user.userProfile
});

const mapDispatchToProps = dispatch => ({
  getUserProfileById: (id) => dispatch(getUserProfileById(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
