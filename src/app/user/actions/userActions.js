export default function getUserProfileById(id) {
  const url = "http://localhost:8080/api/user-profiles/" + id;
  return (dispatch) => {
    fetch(url)
      .then(response => response.json())
      .then(result => {
        dispatch({
          type: 'GET_USER_PROFILE_BY_ID',
          payload: result
        })
      })
  };

  // return {
  //   type: 'GET_USER_PROFILE',
  //   payload: {
  //     name: 'kkk',
  //     gender: 'male',
  //     description: 'ooo'
  //   }
  // };
};
