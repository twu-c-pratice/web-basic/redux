const initState = {
  userProfile: {}
};

export default function nameReducer(state = initState, action) {
  switch (action.type) {
    case 'GET_USER_PROFILE_BY_ID':
      return {
        ...state,
        userProfile: action.payload
      };

    default:
      return state;
  }
}
