import React from 'react';
import ProductDetails from "./product/pages/ProductDetails";
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import Home from "./home/Home";
import UserProfile from "./user/pages/UserProfile";

const App = () => {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route path="/product-details" component={ProductDetails}/>
          <Route path="/user-profile/:id" component={UserProfile}/>
        </Switch>
      </Router>
    </div>
  );
};

export default App;